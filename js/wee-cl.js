/*! wee Conditional Loader. (c) 2016 Walter Ebert. Licensed MIT */
var wee_cl = {
	load_file: function( path, type ) {
		if ( 'css' === type ) {
			loadCSS( path, document.getElementById( 'wee-conditional-loader' ) );
		} else if ( 'js' === type ) {
			loadJS( path );
		}
	},
	load_by_selector: function( selector, path, type ) {
		if ( document.querySelector( selector ) ) {
			wee_cl.load_file( path, type );
		}
	},
	load: function( type, selector, path ) {
		if ( ! 'querySelector' in document ) {
			/* Really old browsers. Load without selector.  */
			wee_cl.load_file( path, type );
		} else  if ( 'addEventListener' in document ) {
			/* Modern browsers */
			document.addEventListener( 'DOMContentLoaded', function() {
				wee_cl.load_by_selector( selector, path, type );
			} );
		} else {
			/* IE8 */
			window.onload = function() {
				wee_cl.load_by_selector( selector, path, type );
			};
		}
	},
	css: function( selector, path ) {
		wee_cl.load( 'css', selector, path );
	},
	js: function( selector, path ) {
		wee_cl.load( 'js', selector, path );
	}
};
