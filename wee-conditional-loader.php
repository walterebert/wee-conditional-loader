<?php
/**
 * Plugin Name: wee Conditional Loader
 * Plugin URI: https://gitlab.com/walterebert/wee-conditional-loader
 * Description: A conditional CSS and JavaScript loader.
 * It uses Filament Group's <a href="https://github.com/filamentgroup/loadCSS">loadCSS</a> and <a href="https://github.com/filamentgroup/loadJS">loadJS</a>.
 *
 * Execution order of requested scripts and styles is not managed, so you should not use it to load multiple files that depend on one another.
 *
 * Version: 0.1.3
 * Author: Walter Ebert
 * Author URI: http://walterebert.com
 * License: MIT
 * License URI: http://opensource.org/licenses/MIT
 * Text Domain: wee-conditional-loader
 * Domain Path: /languages
 *
 * @package wee_Conditional_Loader
 */

/* Deny direct access */
if ( ! function_exists( 'add_filter' ) ) {
	header( 'HTTP/1.1 403 Forbidden' );
	die( 'Access denied' );
}

/* Define plugin constants */
if ( ! defined( 'WEE_CL_LOAD_INLINE' ) ) {
	define( 'WEE_CL_LOAD_INLINE', true );
}
define( 'WEE_CL_BASENAME', plugin_basename( __FILE__ ) );
define( 'WEE_CL_DIR_PATH', plugin_dir_path( __FILE__ ) );
define( 'WEE_CL_DIR_PATH_BASENAME', basename( WEE_CL_DIR_PATH ) );

/**
 * Loader class
 */
class wee_Conditional_Loader {
	/**
	 * @var array $scripts List of JavaScript file names
	 */
	private $scripts = array( 'combined.min.js' );

	/**
	 * Deny direct access
	 */
	private function __construct() {
		if ( WP_DEBUG ) {
			$this->scripts = array( 'loadCSS.min.js', 'loadJS.min.js', 'wee-cl.js' );
		}
	}

	/**
	 * Load CSS
	 *
	 * @param string $selector CSS selector
	 * @param string $path File path
	 * @return string
	 */
	static function css( $selector, $path ) {
		return '<script>wee_cl.css( "' . esc_attr( $selector ) . '", "' . esc_attr( $path ) . '" );</script>'.
		       '<noscript><link rel="stylesheet" type="text/css" href="' . esc_attr( $path ) . '"></noscript>';
	}

	/**
	 * Load JavaScript
	 *
	 * @param string $selector CSS selector
	 * @param string $path File path
	 * @return string
	 */
	static function js( $selector, $path ) {
		return '<script>wee_cl.js( "' . esc_attr( $selector ) . '", "' . esc_attr( $path ) . '" );</script>';
	}

	/**
	 * Load plugin
	 *
	 * @return object Class instance
	 */
	static function load() {
		$instance = new self;

		$instance->load_textdomain();

		/* Load JavaScript inline or not */
		if ( WEE_CL_LOAD_INLINE || empty( $_SERVER['SERVER_PROTOCOL'] ) || ( isset( $_SERVER['SERVER_PROTOCOL'] ) && strpos( $_SERVER['SERVER_PROTOCOL'], 'HTTP/1.' ) === 0 ) ) {
			add_action( 'wp_head', array( $instance, 'scripts_inline' ), 7, 0 );
		} else {
			add_action( 'wp_enqueue_scripts', array( $instance, 'scripts_external' ), 7, 0 );
		}

		return $instance;
	}

	/**
	 * Get the content of a script as a string
	 *
	 * @param string $filename File name.
	 * @return string JavaScript code
	 */
	private static function get_script( $filename ) {
		$file = WEE_CL_DIR_PATH . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . $filename;
		if ( file_exists( $file ) ) {
			return file_get_contents( $file ) . "\n";
		}

		return '';
	}

	/**
	 * Load translations
	 *
	 * @return void
	 */
	function load_textdomain() {
		load_plugin_textdomain( 'wee-conditional-loader', false, WEE_CL_DIR_PATH_BASENAME . '/languages/' );
	}

	/**
	 * Load plugin JavaScript as external files
	 *
	 * @return void
	 */
	function scripts_external() {
		if ( $this->scripts ) {
			foreach ( $this->scripts as $script ) {
				wp_enqueue_script( sanitize_key( $script ), plugins_url( 'js/' . $script, __FILE__ ) );
			}
		}
	}

	/**
	 * Load plugin JavaScript inline head
	 *
	 * @return void
	 */
	function scripts_inline() {
		if ( $this->scripts ) {
			echo "\n<script id=\"wee-conditional-loader\">\n";
			foreach ( $this->scripts as $script ) {
				echo self::get_script( $script );
			}
			echo "</script>\n";
		}
	}
}

/* Load plugin */
add_action( 'plugins_loaded', 'Wee_Conditional_Loader::load' );
